# SRE Exam

## Thanks for giving me this opportunity! 

Happy to use GitLab in the future, but since this is a time sensitive test I decided to use the platform I'm most comfortable with for now.

Project URL:
https://github.com/melscoop/gke-podinfo-cluster

Deployment run:
https://github.com/melscoop/gke-podinfo-cluster/actions/runs/6865454641/job/18669504859
